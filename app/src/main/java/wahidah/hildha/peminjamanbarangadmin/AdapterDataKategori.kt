package wahidah.hildha.uas2

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_kategori.*
import wahidah.hildha.peminjamanbarangadmin.R

class AdapterDataKategori (val dataKtg : List<HashMap<String,String>>,
                           val kategoriActivity: KategoriActivity) :
    RecyclerView.Adapter<AdapterDataKategori.HolderDataKtg>() {
    override fun getItemCount(): Int {
        return dataKtg.size
    }

    override fun onBindViewHolder(p0: AdapterDataKategori.HolderDataKtg, p1: Int) {
        val data = dataKtg.get(p1)
        p0.txKode.setText(data.get("kode_kategori"))
        p0.txNama.setText(data.get("nama_kategori"))

        //begin new
        if (p1.rem(2) == 0) p0.cLayout1.setBackgroundColor(
            Color.rgb(230, 245, 240))
        else p0.cLayout1.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout1.setOnClickListener(View.OnClickListener {
            kategoriActivity.edKodeKat.setText(data.get("kode_kategori"))
            kategoriActivity.edNamaKategori.setText(data.get("nama_kategori"))
        })

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataKategori.HolderDataKtg {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_kategori,p0,false)
        return  HolderDataKtg(v)
    }

    class HolderDataKtg(v : View) : RecyclerView.ViewHolder(v) {
        val txKode = v.findViewById<TextView>(R.id.txKode)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val cLayout1 = v.findViewById<ConstraintLayout>(R.id.cLayout1)
    }
}