package wahidah.hildha.peminjamanbarangadmin

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_peminjaman.*
import org.json.JSONArray
import kotlin.collections.HashMap

class PeminjamanActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imagektp -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent, mediaHelper.getRcGallery())
            }
//            R.id.btnInsert -> {
//                queryInsertUpdateDelete("insert")
//            }
//            R.id.btnUpdate -> {
//                queryInsertUpdateDelete("update")
//            }
//            R.id.btnDelete -> {
//                queryInsertUpdateDelete("delete")
//            }
        }
    }

    lateinit var mediaHelper: MediaHelper
    lateinit var pjmAdapter : AdapterDataPeminjaman
    lateinit var barangAdapter : AdapterDataBarang
    var daftarPeminjaman = mutableListOf<HashMap<String,String>>()
    var daftarBarang = mutableListOf<HashMap<String,String>>()
    lateinit var bitmap : Bitmap

    val url = "http://192.168.43.81/pinjambarang/show_data_peminjaman.php"
    val url2 = "http://192.168.43.81/pinjambarang/get_nama_barang.php"
    var imStr = ""
    var pilihKategori = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_peminjaman)
        pjmAdapter = AdapterDataPeminjaman(daftarPeminjaman, this)
        //barangAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, daftarKategori)
        //spinKategori.adapter = kategoriAdapter
        mediaHelper = MediaHelper(this)
        listPeminjaman.layoutManager = LinearLayoutManager(this)
        listPeminjaman.adapter = pjmAdapter

//        spinKategori.onItemSelectedListener = itemSelected
//        imageView2.setOnClickListener(this)
//        btnInsert.setOnClickListener(this)
//        btnUpdate.setOnClickListener(this)
//        btnDelete.setOnClickListener(this)
//        btngenerate.setOnClickListener(this)
//        initView()
//        cekpermission()
    }


    override fun onStart() {
        super.onStart()
        showDataMhs("")
        getNamaProdi()
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == mediaHelper.getRcGallery()) {
                imStr = mediaHelper.getBitmapToString(data!!.data, imageView2)
            }}
    }



    fun getNamaProdi(){
        val request = StringRequest(
            Request.Method.POST, url2,
            Response.Listener {
                    response ->
                daftarBarang.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var brg = HashMap<String,String>()
                    brg.put("nama_barang",jsonObject.getString("nama_barang"))
                }
                //barangAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener {
                    error ->
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun showDataMhs(namaPjm: String){
        val request = object : StringRequest(
            Request.Method.POST, url, Response.Listener{ response ->
                daftarPeminjaman.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var brg = HashMap<String,String>()
                    brg.put("id_pinjam",jsonObject.getString("id_pinjam"))
                    brg.put("nama_pinjam",jsonObject.getString("nama_pinjam"))
                    brg.put("no_hp",jsonObject.getString("no_hp"))
                    brg.put("tgl_pinjam",jsonObject.getString("tgl_pinjam"))
                    brg.put("nama_barang",jsonObject.getString("nama_barang"))
                    brg.put("status",jsonObject.getString("status"))
                    brg.put("url",jsonObject.getString("url"))
                    daftarPeminjaman.add(brg)
                }
                pjmAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener{ error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("nama_pinjam", namaPjm)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}