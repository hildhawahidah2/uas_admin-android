package wahidah.hildha.peminjamanbarangadmin

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidmads.library.qrgenearator.QRGSaver
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.zxing.WriterException
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imageView2 -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent, mediaHelper.getRcGallery())
            }
            R.id.btnInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete -> {
                queryInsertUpdateDelete("delete")
            }
        }
    }

    lateinit var mediaHelper: MediaHelper
    lateinit var brgAdapter : AdapterDataBarang
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var daftarBarang = mutableListOf<HashMap<String,String>>()
    var daftarKategori = mutableListOf<String>()
    lateinit var bitmap : Bitmap

    val url = "http://192.168.43.81/pinjambarang/show_data_barang.php"
    val url2 = "http://192.168.43.81/pinjambarang/get_nama_kategori.php"
    val url3 = "http://192.168.43.81/pinjambarang/query_IUD_barang.php"
    var imStr = ""
    var pilihKategori = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        brgAdapter = AdapterDataBarang(daftarBarang, this)
        kategoriAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, daftarKategori)
        spinKategori.adapter = kategoriAdapter
        mediaHelper = MediaHelper(this)
        listBarang.layoutManager = LinearLayoutManager(this)
        listBarang.adapter = brgAdapter

        spinKategori.onItemSelectedListener = itemSelected
        imageView2.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btngenerate.setOnClickListener(this)
        initView()
        cekpermission()
    }


    override fun onStart() {
        super.onStart()
        showDataMhs("")
        getNamaProdi()
    }


    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinKategori.setSelection(0)
            pilihKategori = daftarKategori.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKategori = daftarKategori.get(position)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == mediaHelper.getRcGallery()) {
                imStr = mediaHelper.getBitmapToString(data!!.data, imageView2)
            }}
    }


    fun cekpermission(){
        if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){

            }else{
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),30)
            }
        }
    }
    private fun initView(){
        btngenerate.setOnClickListener{
            var kode = edKode.text
            var nama = edNamaBarang.text
            var XML = "{'kode_barang':'" + kode + "','nama_barang':'" + nama + "'}";
            val qrgenerator = QRGEncoder(XML, null, QRGContents.Type.TEXT, 300)
            try {
                bitmap = qrgenerator.encodeAsBitmap()
                imageView2.setImageBitmap(bitmap)
            }catch (e: WriterException){

            }
        }
        btnsave.setOnClickListener{
            val savePath = Environment.getExternalStorageDirectory().getPath() + "/Qrcode/";
            QRGSaver.save(savePath,"QR"+" "+edNamaBarang.text,bitmap, QRGContents.ImageType.IMAGE_JPEG)
            Toast.makeText(this,"QR CODE Telah Disimpan", Toast.LENGTH_SHORT).show()
            imageView2.setImageBitmap(null)
        }
    }

    fun getNamaProdi(){
        val request = StringRequest(
            Request.Method.POST, url2,
            Response.Listener {
                    response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKategori.add(jsonObject.getString("nama_kategori"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener {
                    error ->
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun showDataMhs(namaBrg: String){
        val request = object : StringRequest(
            Request.Method.POST, url, Response.Listener{ response ->
                daftarBarang.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var brg = HashMap<String,String>()
                    brg.put("kode_barang",jsonObject.getString("kode_barang"))
                    brg.put("nama_barang",jsonObject.getString("nama_barang"))
                    brg.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    brg.put("url",jsonObject.getString("url"))
                    daftarBarang.add(brg)
                }
                brgAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener{ error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("nama_barang", namaBrg)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST, url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")){
                    showDataMhs("")

                } else {
                    Toast.makeText(this, "Gagal melakukan Operasi", Toast.LENGTH_LONG).show()
                }
                brgAdapter.notifyDataSetChanged()
                when(mode){
                    "insert" ->{
                        Toast.makeText(this, "Berhasil insert data", Toast.LENGTH_LONG).show()
                    }
                    "update" ->{
                        Toast.makeText(this, "Berhasil update data", Toast.LENGTH_LONG).show()
                    }
                    "delete" ->{
                        Toast.makeText(this, "Berhasil delete data", Toast.LENGTH_LONG).show()
                    }
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()

            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                val nmFile = "QR"+edNamaBarang.text+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode", "insert")
                        hm.put("kode_barang", edKode.text.toString())
                        hm.put("nama_barang", edNamaBarang.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                        hm.put("nama_kategori", pilihKategori)
                    }
                    "update" ->{
                        hm.put("mode", "update")
                        hm.put("kode_barang", edKode.text.toString())
                        hm.put("nama_barang", edNamaBarang.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                        hm.put("nama_kategori", pilihKategori)
                    }
                    "delete" ->{
                        hm.put("mode", "delete")
                        hm.put("kode_barang", edKode.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}