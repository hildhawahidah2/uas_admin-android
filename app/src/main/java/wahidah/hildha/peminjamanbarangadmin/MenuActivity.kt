package wahidah.hildha.peminjamanbarangadmin

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity: AppCompatActivity(), View.OnClickListener {

    val BRG : Int = 100
    val KTG : Int = 101
    val PNJM : Int = 101


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button ->{
                var intent = Intent(this,KategoriActivity::class.java)
                startActivityForResult(intent,BRG)
            }
            R.id.button2 ->{
                var intent = Intent(this,MainActivity::class.java)
                startActivityForResult(intent,KTG)
            }
            R.id.button3 ->{
                var intent = Intent(this,PeminjamanActivity::class.java)
                startActivityForResult(intent,PNJM)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        button.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)


    }


}