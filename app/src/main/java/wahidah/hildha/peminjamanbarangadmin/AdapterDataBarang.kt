package wahidah.hildha.peminjamanbarangadmin

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterDataBarang (val dataBrg : List<HashMap<String,String>>,
                         val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterDataBarang.HolderDataBrg>() {
    override fun getItemCount(): Int {
        return dataBrg.size
    }

    override fun onBindViewHolder(p0: AdapterDataBarang.HolderDataBrg, p1: Int) {
        val data = dataBrg.get(p1)
        p0.txKode.setText(data.get("kode_barang"))
        p0.txNama.setText(data.get("nama_barang"))
        p0.txKategori.setText(data.get("nama_kategori"))

        //begin new
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230, 245, 240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            val pos = mainActivity.daftarKategori.indexOf(data.get("nama_kategori"))
            mainActivity.spinKategori.setSelection(pos)
            mainActivity.edKode.setText(data.get("kode_barang"))
            mainActivity.edNamaBarang.setText(data.get("nama_barang"))
            Picasso.get().load(data.get("url")).into(mainActivity.imageView2);
        })

        //end new


        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataBarang.HolderDataBrg {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_barang,p0,false)
        return  HolderDataBrg(v)
    }

    class HolderDataBrg(v : View) : RecyclerView.ViewHolder(v) {
        val txKode = v.findViewById<TextView>(R.id.txKode)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txKategori = v.findViewById<TextView>(R.id.txKategori)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}