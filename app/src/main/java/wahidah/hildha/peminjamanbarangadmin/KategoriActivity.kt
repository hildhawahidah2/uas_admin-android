package wahidah.hildha.uas2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kategori.*
import org.json.JSONArray
import org.json.JSONObject
import wahidah.hildha.peminjamanbarangadmin.R
import kotlin.collections.HashMap

class KategoriActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnInsert1 -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate1 -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete1 -> {
                queryInsertUpdateDelete("delete")
            }
        }
    }

    lateinit var ktgAdapter : AdapterDataKategori
    var daftarKategori = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.81/pinjambarang/show_data_kategori.php"
    val url3 = "http://192.168.43.81/pinjambarang/query_IUD_kategori.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kategori)
        ktgAdapter = AdapterDataKategori(daftarKategori, this)
        listKategori.layoutManager = LinearLayoutManager(this)
        listKategori.adapter = ktgAdapter
        btnInsert1.setOnClickListener(this)
        btnUpdate1.setOnClickListener(this)
        btnDelete1.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataMhs("")
    }

    fun showDataMhs(namaBrg: String){
        val request = object : StringRequest(
            Request.Method.POST, url, Response.Listener{ response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var brg = HashMap<String,String>()
                    brg.put("kode_kategori",jsonObject.getString("kode_kategori"))
                    brg.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    daftarKategori.add(brg)
                }
                ktgAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener{ error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("nama_kategori", namaBrg)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST, url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")){
                    //Toast.makeText(this, "Berhasil", Toast.LENGTH_LONG).show()
                    showDataMhs("")

                } else {
                    Toast.makeText(this, "Gagal melakukan Operasi", Toast.LENGTH_LONG).show()
                }
                when(mode){
                    "insert" ->{
                        Toast.makeText(this, "Berhasil insert data", Toast.LENGTH_LONG).show()
                    }
                    "update" ->{
                        Toast.makeText(this, "Berhasil update data", Toast.LENGTH_LONG).show()
                    }
                    "delete" ->{
                        Toast.makeText(this, "Berhasil delete data", Toast.LENGTH_LONG).show()
                    }
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()

            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode", "insert")
                        hm.put("kode_kategori", edKodeKat.text.toString())
                        hm.put("nama_kategori", edNamaKategori.text.toString())
                    }
                    "update" ->{
                        hm.put("mode", "update")
                        hm.put("kode_kategori", edKodeKat.text.toString())
                        hm.put("nama_kategori", edNamaKategori.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode", "delete")
                        hm.put("kode_kategori", edKodeKat.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}

