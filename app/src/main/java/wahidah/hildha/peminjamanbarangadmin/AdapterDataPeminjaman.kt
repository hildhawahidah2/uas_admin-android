package wahidah.hildha.peminjamanbarangadmin

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataPeminjaman (val dataPjm : List<HashMap<String,String>>,
                             val peminjamanActivity: PeminjamanActivity) :
    RecyclerView.Adapter<AdapterDataPeminjaman.HolderDataPjm>() {
    override fun getItemCount(): Int {
        return dataPjm.size
    }

    override fun onBindViewHolder(p0: AdapterDataPeminjaman.HolderDataPjm, p1: Int) {
        val data = dataPjm.get(p1)
        p0.txId.setText(data.get("id_pinjam"))
        p0.txNama.setText(data.get("nama_pinjam"))
        p0.txNo.setText(data.get("no_hp"))
        p0.txTgl.setText(data.get("tgl_pinjam"))
        p0.txBarang.setText(data.get("nama_barang"))
        p0.txStatus.setText(data.get("status"))


        //begin new
        if (p1.rem(2) == 0) p0.clayout2.setBackgroundColor(
            Color.rgb(230, 245, 240))
        else p0.clayout2.setBackgroundColor(Color.rgb(255, 255, 245))

//        p0.clayout2.setOnClickListener(View.OnClickListener {
//            val pos = peminjamanActivity.daftarPeminjaman.indexOf(data.get("nama_kategori"))
//            peminjamanActivity.spinKategori.setSelection(pos)
//            peminjamanActivity.edKode.setText(data.get("kode_barang"))
//            peminjamanActivity.edNamaBarang.setText(data.get("nama_barang"))
//            Picasso.get().load(data.get("url")).into(peminjamanActivity.imagektp);
//        })

        //end new


        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataPeminjaman.HolderDataPjm {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_peminjaman,p0,false)
        return  HolderDataPjm(v)
    }

    class HolderDataPjm(v : View) : RecyclerView.ViewHolder(v) {
        val txId = v.findViewById<TextView>(R.id.textid)
        val txNama = v.findViewById<TextView>(R.id.textnama)
        val txNo = v.findViewById<TextView>(R.id.textno)
        val txTgl = v.findViewById<TextView>(R.id.texttgl)
        val txBarang = v.findViewById<TextView>(R.id.textbarang)
        val txStatus= v.findViewById<TextView>(R.id.textstatus)
        val photo = v.findViewById<ImageView>(R.id.imagektp)
        val clayout2 = v.findViewById<ConstraintLayout>(R.id.clayout2)
    }
}